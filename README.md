# โปรเจกต์ร้านขายนาฬิกาออนไลน์ใช้ React.js

## การติดตั้งโปรเจกต์

### ติดตั้งแพคเกจที่จำเป็นทั้งหมด

### To intsall all the required dependecies

```
npm install
```

### เริ่มโปรเจกต์ 
## Start the project

```
npm start
```

เปิดลิงก์ http://localhost:3000 ในเบราว์เซอร์เพื่อดูโปรเจกต์

### Test the application

```
npm test
```

### รูปตัวอย่างโปรเจกต์

[![1.png](https://i.postimg.cc/hPZ3rq0t/1.png)](https://postimg.cc/jLNXxm9p)
[![2.png](https://i.postimg.cc/GmBggSMJ/2.png)](https://postimg.cc/yDHTknCk)